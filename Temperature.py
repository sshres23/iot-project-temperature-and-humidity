#!/usr/bin/python
import time
import logging
from datetime import datetime
from sense_hat import SenseHat
import smtplib

#### DISPLAY COLOR TEST ####
sense = SenseHat()
r = 255
g = 255
b = 255
sense.clear((r, g, b))
time.sleep(1)
r = 255
g = 0
b = 0
sense.clear((r, g, b))
time.sleep(1)
r = 0
g = 255
b = 0
sense.clear((r, g, b))
time.sleep(1)
r = 0
g = 0
b = 255
sense.clear((r, g, b))
time.sleep(1)
r = 0
g = 0
b = 0
sense.clear((r, g, b))
#### Preparing Logging ####
root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
today = datetime.today()
date = today.strftime("%b-%d-%Y--%H-%M-%S")
handler = logging.FileHandler(date+".log","w", "utf-8")
handler.setFormatter(logging.Formatter('%(asctime)s - %(message)s'))
root_logger.addHandler(handler)
####    Grabbing Temperature   ####
red=(255,0,0)
grey=(128,128,128)


#Preparing Logging
root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
today = datetime.today()
date = today.strftime("%b-%d-%Y--%H-%M-%S")
handler = logging.FileHandler("enterloglocation here"+date+".log","w", "utf-8")
handler.setFormatter(logging.Formatter('%(asctime)s - %(message)s'))
root_logger.addHandler(handler)


temp = ('%.3f'% sense.temperature)
red=(255,0,0)
grey=(128,128,128)
sense.show_message(temp, text_colour=red, back_colour=grey)

logging.debug("The temperature for the room was " + temp )
sense.clear()
        

        


##Sending the email
#Email Variables
SMTP_SERVER = 'smtp.gmail.com' #Email Server (don't change!)
SMTP_PORT = 587 #Server Port (don't change!)
GMAIL_USERNAME ='@EmailAddress'
GMAIL_PASSWORD ='Password'

class Emailer:
    def sendmail(self, recipient, subject, content):
         
        #Create Headers
        headers = ["From: " + GMAIL_USERNAME, "Subject: " + subject, "To: " + recipient,
                   "MIME-Version: 1.0", "Content-Type: text/html"]
        headers = "\r\n".join(headers)
 
        #Connect to Gmail Server
        session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        session.ehlo()
        session.starttls()
        session.ehlo()
 
        #Login to Gmail
        session.login(GMAIL_USERNAME, GMAIL_PASSWORD)
 
        #Send Email & Exit
        session.sendmail(GMAIL_USERNAME, recipient, headers + "\r\n\r\n" + content)
        session.quit
 

sender = Emailer()
sendTo = '@EmailAddress'
emailSubject = "IOT Research: Temperature Sensor " + date
emailContent = "The temperature of the room was "+ temp

#Sends an email to the "snedTo" address with the sp
sender.sendmail(sendTo, emailSubject, emailContent)
